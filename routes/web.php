<?php

use Illuminate\Support\Facades\Route;
use app\Http\Controllers\DataController;
use app\Http\Controllers\LanguageController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('/data', 'DataController');
Route::resource('/language', 'LanguageController');
Route::resource('/categories', 'CategoryController');
Route::resource('/homepage', 'HomepageController');