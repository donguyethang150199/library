<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Language;
use App\Models\Category;

class Data extends Model
{
   
    public $timestamps = false;
    protected $table = 'data';
    protected $fillable = [
        'name', 'amount', 'publisher','year','image_path'
    ];
    
    public function languages()
    {
        return $this->belongsToMany(Language::class, 'data_language','language_id', 'data_id');
    }

    public function categories()
        {
            return $this->belongsToMany(Category::class,'category_data', 'category_id', 'data_id');
        }
    
}

