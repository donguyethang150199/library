<?php

namespace App\Http\Controllers;

use App\Models\Data;
use App\Models\Language;
use App\Models\Category;
use App\Http\Requests\CreateDataRequest;
use App\Http\Requests\UpdateDataRequest;
use Illuminate\Http\Request;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $datas = Data::with('categories','languages')->paginate(5);

        return view('data.index',compact('datas'));
            
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        $languages = Language::get();
        $categories = Category::get();
        return view('data.create', compact('categories','languages'));   
    }
        
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDataRequest $request)
    { //submit
        $request->all();
        $image = request()->file('image_path');
        $newImageName = time() . '-' . $request->name . '.' . 
        $request->image->extension();
        $request->image->move(public_path('images'), $newImageName);

        $data = Data::create([
            'name' => $request->input('name'),
            'amount' => $request->input('amount'),
            'publisher'=>$request->input('publisher'),
            'year'=>$request->input('year'),
            'image_path'=> $newImageName ,
            ]);
        $data->languages()->attach($request->input('languages')); 
        $data->categories()->attach($request->input('categories')); 
        return redirect()->route('data.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Data $data)
    {  
       return view('data.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    { //vao trang edit
        $data = Data::findOrFail($id); 
        $languages = Language::get();
        $categories = Category::get();
        
        $selectedLanguages =  $data->languages()->pluck('languages.id')->toArray();
        $selectedCategories = $data->categories()->pluck('categories.id')->toArray();
        return view('data.edit',compact('data','languages','categories','selectedCategories', 'selectedLanguages'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(UpdateDataRequest $request, $id)
    { 

        $data=Data::findOrFail($id);
        $request->all(); 
        
        $data->name = $request->input('name');
        $data->amount = $request->input('amount');
        $data->publisher = $request->input('publisher');
        $data->year = $request->input('year');       

        if($request->hasFile('image_path')) {
            $image = $request->file('image_path');
            $extension = $image->getClientOriginalExtension();
            $filename = time() . '-' . $request->name . '.' .$extension;    
            $image->move(public_path('images'),$filename);
            $data->image_path = $filename;
        }
        $data->update(); //save
        $data->languages()->sync($request->input('languages'));  
        $data->categories()->sync($request->input('categories'));  
       
        return redirect()->route('data.index')->with('success','Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        Data::destroy($id);
        return redirect()->route('data.index');
    }
}
