
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome to Dehanu Library</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<body>

<style>

body, html{
    margin:0;
    padding:0;
    font-family:Palatino Linotype;
    width:100%;
    height:100%;
}
.section-top{
    width:100%;
    height:100%;
    overflow: hidden;
    position: relative;
    background-image: url('https://b-f33-zpg.zdn.vn/1248775459659432309/4c93f2f0a05953070a48.jpg');
    background-repeat: no-repeat;
    background-size: cover;
    animation: slide 9s infinite;
}
.content{
    position: absolute;
    top:50%;
    left:50%;
    transform:translate(-50%,-50%);
    text-transform: uppercase;
    text-align: center;
}
.content h1{
    color: white;
    font-size: 50px;
    letter-spacing: 13px;
    -webkit-text-stroke-width: 0.5px;
    -webkit-text-stroke-color: SaddleBrown;
}
a{
    background: #ffc107;
    color: black;
    text-decoration: none;
    font-size: 20px;
    border-radius: 10px; 
}
.block{
    margin-left: auto;
    margin-right: auto;
    width: 1200px;    
}
@keyframes slide {
    0%{
        background-image: url(https://b-f39-zpg.zdn.vn/4864215506975026854/320cde7b8cd27f8c26c3.jpg);
    }
    33%{
        background-image: url(https://b-f34-zpg.zdn.vn/9093405869168826055/e58bc1e3934a6014395b.jpg);
    }
    67%{
        background-image: url(https://images.wallpaperscraft.com/image/clouds_mountains_art_127406_3840x2160.jpg);
    }
    100%{
        background-image: url(https://b-f39-zpg.zdn.vn/4864215506975026854/320cde7b8cd27f8c26c3.jpg);
    }
}

</style>


<div class="section-top">
    <div class="content">
        <img src="https://i.pinimg.com/originals/d0/94/77/d0947720fc61486261ca30705cc4ff98.png" alt="logo" width="200" height="200">
        <h1> DEHANU LIBRARY </h1>
        <a href="{{ route('data.index') }}">Access to Library<a>
    </div>
</div>
<br>

<div class="block" style="max-width:1000px; text-align:center; font-color: rgb(88,0,0);">
  <h2 style="font-size:50px">Librarians</h2>
  <p>We have created a fictional library website. </p>
</div>

<br>

<div class="container">
  <div class="row" style="text-align:center;">
    <div class="col">
    <p>Huyen</p>
    <img src="https://i.pinimg.com/originals/e9/f7/bc/e9f7bc270179f6a3a6ef752322eb4b39.png" alt="Random Name" style="width:100%">
    
    </div>
    <div class="col">
    <p>Hang</p>
    <img src="https://i.pinimg.com/originals/73/32/3b/73323b4393bf7adf620cbe820b0ae70b.png" alt="Random Name" style="width:100%">
    
    </div>
    <div class="col">
    <p>An</p>
    <img src="https://i.pinimg.com/originals/5a/32/8e/5a328e43e06fda5ffc1fc4cc16d380b0.png" alt="Random Name" style="width:100%">
    
    </div>
</div>
</html>
</body>

</html>