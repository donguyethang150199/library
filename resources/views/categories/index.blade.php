@extends('categories.layout')

@section('content')
    <div class="container">
    <br>
    <br>
        <div style="text-align: center;"><h1>Book genres</h1></div>
        <br>
        <br>
    <div class="row-justify-content-center" style="text-align: center; font-family: Palatino Linotype;">
        <div class="col-lg-12 margin-tb">
           <div class="pull-right">
               <a class="btn btn-success" href="{{ route('categories.create') }}"> Create</a>
           </div>
       </div>
    </div>
    <br>
    <br>

        @if ($message = Session::get('success'))
       <div class="alert alert-success">
           <p>{{ $message }}</p>
       </div>
        @endif

        <table class="table table-bordered">
            <tr>
                <td>No</td>
                <td>Genre</td>
                <td>Actions</td>
            </tr>
            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                        <form action="{{ route('categories.destroy', $category->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a class="btn btn-primary" href="{{ route('categories.edit', $category->id) }}">Edit</a>
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $categories->links() }}
    </div>
@endsection
