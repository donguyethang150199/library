@extends('data.layout1')
@section('content')
 
   @if ($message = Session::get('success'))
       <div class="alert alert-success">
           <p>{{ $message }}</p>
       </div>
   @endif 
   <table class="table table-bordered">
   <br>
       <tr>
           <th>No</th>
           <th>Image</th>
           <th>Name</th>
           <th>Amount</th>
           <th>Publisher</th>
           <th>Year</th>
           <th>Language</th>
           <th>Genre</th>
           <th width="280px">Action</th>
       </tr>
       @foreach ($datas as $i => $data)
       <tr>
            <td>{{ $i + 1 }}</td>
           <td>
           <img
            src="{{ asset('images/'. $data->image_path)}}"
            width="100" height="120"
            alt=""
            >
            </td> 
           <td>{{ $data->name }}</td>
           <td>{{ $data->amount }}</td>
           <td>{{ $data->publisher }}</td>
           <td>{{ $data->year }}</td>
           <td>
           @foreach($data->languages as $language) {{$language->name}}
            @if (!$loop->last) , @endif 
           @endforeach
           </td>
           <td>@foreach($data->categories as $category) {{ $category->name }}
           @if (!$loop->last) , @endif 
           @endforeach
           </td>
           <td>
        <form action="{{ route('data.destroy',$data->id) }}" method="POST">

            <a class="btn btn-warning" href="{{ route('data.show',$data->id) }}">Detail</a>

            <a class="btn btn-success" href="{{ route('data.edit',$data->id) }}">Edit</a>

            @csrf
            @method('DELETE')

            <button type="submit" onclick="return confirm ('Do you really want to delete?')" class="btn btn-danger" >Delete</button>
        </form>
           </td>
       </tr>
       @endforeach
   </table>
   <div class="pagination justify-content-center">
{!! $datas->links('pagination::bootstrap-4') !!}
   </div>


@endsection
