@extends('data.layout')
  
@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
    <br>
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>BOOK'S INFORMATION</h3>
            </div>
        </div>
    </div>
   
    <div class="row">
    <div class="col-sm-6">
        <img style="margin:35px 80px;"
            src="{{ asset('images/'. $data->image_path)}}"
            width="320" height="420"
            alt=""
        >
    </div>
        <div class="col-sm-4" style="margin:100px 3px ; background-color:rgba(20, 20, 20, 0.3) ; padding: 25px 20px 20px 30px ; border-radius:20px; box-shadow : 4px 4px 4px orange">

        <div class="text-center">
            <div class="form-group">
                <strong>Name:</strong>
                <strong>{{ $data->name }}</strong>
            </div>
        
            <div class="form-group">
                <strong>Amount:</strong>
                <strong>{{ $data->amount }}</strong>
            </div>
        
            <div class="form-group">
                <strong>Publisher:</strong>
                <strong>{{ $data->publisher }}</strong>
            </div>
       
            <div class="form-group">
                <strong>Year:</strong>
                <strong>{{ $data->year}}</strong>
            </div>
            <div class="form-group">
                <strong>Language:</strong>
                <strong>@foreach($data->languages as $language)
            {{$language->name}}
            @if (!$loop->last), @endif  @endforeach
            </strong>                
            </div>
            <div class="form-group">
                <strong>Genre:</strong>
                <strong>@foreach($data->categories as $category)
            {{ $category->name}} @if (!$loop->last), @endif @endforeach
                </strong>
            </div>
            <div class="text-center" style="font-family: Palatino Linotype;">
                <a class="btn btn-warning" href="{{ route('data.index') }}">Back</a>
            </div>
            </div>
        </div>
        
        
    </div>
@endsection