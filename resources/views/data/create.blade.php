@extends('data.layout')
  
@section('content')
<br>
<br>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <h3>ADD BOOK</h3>
        </div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('data.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    
    
    <div class="block" style="font-family: Palatino Linotype; width:400px; text-align:center;">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name" >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Amount:</strong>
                <input type="number" class="form-control" name="amount" placeholder="Amount"></input>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Publisher:</strong>
                <input type="text" name="publisher" class="form-control" placeholder="Publisher">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Year:</strong>
                <input type="number" name="year" class="form-control" placeholder="Year">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <strong>Language:</strong>
            <select name="languages[]" class="form-control" multiple>
                @foreach($languages as $language)
                    <option value="{{ $language->id }}">{{ $language->name }}</option>
                @endforeach
            </select>
            <br>
                    <strong>Genre:</strong>
                    <select name="categories[]" class="form-control" multiple>
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <br>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <div class="form-group">
                <strong>Image:</strong>
                <input type="file" name="image">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center"> 
        <a class="btn btn-warning" href="{{ route('data.index') }} " style="font-family:Palatino Linotype;"> Back</a>
        <button type="submit" class="btn btn-success" style="font-family:Palatino Linotype;">Submit</button>
        </div>
    </div>
    
   
</form>
@endsection

