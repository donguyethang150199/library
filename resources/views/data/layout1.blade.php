<!DOCTYPE html>
<html>
    <head>
        <title>DeHanu Library</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"/>
    </head>
<body>
<div class="container">
<div class="col-xs-12 col-sm-12 col-md-12 text-center">
       <div class="col-lg-12 margin-tb">
       
           <div class="row">
                <div class="col-sm-2" style="margin:15px 20px">
                    <img src="https://media0.giphy.com/media/1v0ySRY3f7gUuSeBIq/giphy.gif" width="200" height="150"
                    alt="">
                </div>
                
                        <div class="col-sm-6" style="margin:40px 50px">
                    <h1>DEHANU LIBRARY</h1>
                    <h5>_Knowledge is power_ <h5>
                       </div>
                
                <div class="col-sm-2" style="margin:15px 10px">
                    <img src="https://media0.giphy.com/media/1v0ySRY3f7gUuSeBIq/giphy.gif" width="200" height="150"
                    alt="">
                </div>
           </div>
           <div class="pull-right" style="font-family:Palatino Linotype;">
               <a class="btn btn-warning" href="{{ route('data.create') }}"> Create new book</a>
           </div>
       </div>
   </div>
   @yield('content')
   </div>
   </div>
<style> 
   h1,h2 {
            font-family: Palatino Linotype;
            font-size: 45px;
            color: white;
            text-shadow: 2px 2px 15px yellow;
    }
    h3 {
        font-family: Palatino Linotype;
        font-size: 35px;
        color: white;
        text-shadow: 2px 2px 5px yellow;
    }
    h5 {
        font-family: Palatino Linotype;
        font-size: 20px;
        color: white;
        text-shadow: 2px 2px 5px yellow;
        font-style: italic;
    }
    label, th, td, strong{ 
        font-family: Palatino Linotype;
        color: white;
    }
    body{
        background-image: url('https://b-f33-zpg.zdn.vn/1248775459659432309/4c93f2f0a05953070a48.jpg');
  		background-repeat: no-repeat;
  		background-attachment: fixed;
  		background-size: 100% 100%;
    }
    table{
        background-color: rgba(20, 20, 20, 0.6);

    }         
</style>
</body>
</html>
