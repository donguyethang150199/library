@extends('data.layout')
   
@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
    <br>
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>EDIT</h3>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('data.update',$data->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id }}">
        <div class="block" style="font-family: Palatino Linotype; width:400px; text-align:center;">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="{{ $data->name }}" class="form-control" >
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Amount:</strong>
                    <input type="number" name="amount" value="{{ $data->amount }}" class="form-control" >
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Publisher:</strong>
                    <input type="text" name="publisher" value="{{ $data->publisher }}" class="form-control">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Year:</strong>
                    <input type="number" name="year" value="{{ $data->year }}" class="form-control">
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div>
                <strong>Language:</strong>
            <select name="languages[]" class="form-control" multiple>
                @foreach($languages as $language)                   
                    <option value="{{ $language->id }}"
                    {{ in_array($language->id, $selectedLanguages) ? 'selected':''}}>
                    {{$language ->name}}
                    </option>
                @endforeach
                </select>
                </div>
                <div>
                <strong>Genre:</strong>
                <select name="categories[]" class="form-control" multiple>
                @foreach($categories as $category)
                <option value="{{ $category->id }}"  
                {{ in_array($category->id, $selectedCategories) ? 'selected' : '' }}>
                {{ $category->name }}
                </option>
                @endforeach
                </select>
                </div>
            </div>
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <input type="file" name="image_path" value="{{$data->image_path}}">
                <label class="custom-file-input">Browse</lable>
            </div>
              <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <a class="btn btn-warning" href="{{ route('data.index') }}">Back</a>
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>

   
    </form>


@endsection