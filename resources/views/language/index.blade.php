@extends('language.layout')


@section('content')
<br>
<br>
<div style="text-align: center"><h1>Languages</h1></div>
<br>
<br>
   <div class="row-justify-content-center" style="text-align: center; font-family: Palatino Linotype;">
       <div class="col-lg-12 margin-tb">
           <div class="pull-right">
               <a class="btn btn-success" href="{{ route('language.create') }}"> Create</a>
           </div>
       </div>
   </div>
   <br>
   <br>
   <table class="table table-bordered">
       <tr>
           <th>No</th>
           <th>Name</th>
           <th width="280px">Action</th>
       </tr>
       @foreach ($languages as $language)
       <tr>
           <td>{{ $language->id }}</td>
           <td>{{ $language->name }}</td>
           <td>
               <form action="{{ route('language.destroy',$language->id) }}" method="POST">

                   <a class="btn btn-primary" href="{{ route('language.edit',$language->id) }}">Edit</a>
 
                   @csrf
                   @method('DELETE')
    
                   <button type="submit" class="btn btn-danger">Delete</button>
               </form>
           </td>
       </tr>
       @endforeach
   </table>
@endsection