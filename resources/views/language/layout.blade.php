<!DOCTYPE html>
<html>
    <head>
        <title>Language</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"/>
    </head>
    <body>
<style>
   
   h1,h2 {
            font-family: Palatino Linotype;
            font-size: 45px;
            color: white;
            text-shadow: 2px 2px 15px yellow;
    }
    h3 {
        font-family: Palatino Linotype;
        font-size: 35px;
        color: white;
        text-shadow: 2px 2px 5px yellow;
    }
    h5 {
        font-family: Palatino Linotype;
        font-size: 20px;
        color: white;
        text-shadow: 2px 2px 5px yellow;
    }
    label, th, td, strong{ 
        font-family: Palatino Linotype;
        color: white;
    }
    body{
        background-image: url('https://b-f33-zpg.zdn.vn/1248775459659432309/4c93f2f0a05953070a48.jpg');
  		background-repeat: no-repeat;
  		background-attachment: fixed;
  		background-size: 100% 100%;
    }
    table{
        background-color: rgba(20, 20, 20, 0.6);

    }
    
      
         
</style>
<div class="container">
    @yield('content')
</div>
</body>
</html>